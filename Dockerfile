From composer

ADD composer.json /app
ADD composer.lock /app
RUN composer install
